/*
-Đầu vào:số a,b,c.
-Xử lý:
+ tạo biến, và ép kiểu cho 3 số a,b,c.
+ Tạo 1 biến trung gian là temp. Nếu a > b thì đổi chỗ a và b bằng cách temp = a, a = b , b = a. Nếu a < b thì giữ nguyên.
+ Nếu a > c. thì tiến hành đổi chỗ a với c như trên, nếu không thì giữ nguyên. Vậy là số bé nhất được gán vào a.
+ So sánh b với c. Nếu b > c thì đổi chỗ b với c. nếu không thì giữ nguyên.
-Đầu ra: 3 số a,b,c được sắp xếp theo thứ tự tăng dần.
*/
function sapXep()
{
    var soA = document.getElementById('txt-so-a').value *1;
    var soB = document.getElementById('txt-so-b').value *1;
    var soC = document.getElementById('txt-so-c').value *1;
    var temp = 0;
    if (soA > soB) {
        temp = soA;
        soA = soB;
        soB = temp;
    }
    if (soA > soC) {
        temp = soA;
        soA = soC;
        soC = temp;
    }
    if (soB > soC){
        temp = soB;
        soB = soC;
        soC = temp;
    }
    document.getElementById("result").innerHTML = `${soA}, ${soB}, ${soC}`;
}